
// interface AttributesType {
//   alt: string
//   src: string
//   title: string
// }

type createElementType = {
  tagName: string
  className?: string
  attributes?: any
}

// type KeyType = "tagName" | "className" | "attributes";
export function createElement({ tagName, className, attributes }:createElementType) {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
