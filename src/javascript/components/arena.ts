import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal} from './modal/winner';

export interface SelectedFightersInterface {
  _id: string
  name: string
  health: number
  attack: number
  defense: number
  source?: string
}

export function renderArena(selectedFighters: SelectedFightersInterface[]) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);
  root.innerHTML = '';
  root.append(arena);
  // - start the fight
  // - when fight is finished show winner
  let winner = fight(selectedFighters[0], selectedFighters[1]);
  winner.then(result => showWinnerModal(result))
}

function createArena(selectedFighters:Array<SelectedFightersInterface>) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(selectedFighters[0], selectedFighters[1]);
  const fighters = createFighters(selectedFighters[0], selectedFighters[1]);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter:SelectedFightersInterface, rightFighter:SelectedFightersInterface) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter:SelectedFightersInterface, position:string) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter:SelectedFightersInterface, secondFighter:SelectedFightersInterface) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter:SelectedFightersInterface, position:string) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
