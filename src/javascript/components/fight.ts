import { controls } from '../../constants/controls';
import { fighters } from '../helpers/mockData';
import {SelectedFightersInterface} from "./arena";

class Fightner {
  name:string;
  attack:number;
    defense: number
    primaryHealth: number
    health:number
    blockPosition:boolean
    extraHitPower:boolean
    helperCountExtraHit:[]
    position: "left-fighter-indicator" | "right-fighter-indicator" | ""
  constructor(fightnerDetails:SelectedFightersInterface) {
    this.name = fightnerDetails.name;
    this.attack = fightnerDetails.attack;
    this.defense = fightnerDetails.defense;
    this.primaryHealth = fightnerDetails.health;
    this.health = fightnerDetails.health;
    this.blockPosition = false;
    this.extraHitPower = true;
    this.helperCountExtraHit = [];
    this.position = '';
  }
}

export async function fight(firstFighter:SelectedFightersInterface, secondFighter:SelectedFightersInterface) {
  return new Promise<SelectedFightersInterface>((resolve) => {
    let firstFighterBoard = new Fightner(firstFighter);
    firstFighterBoard.position = 'left-fighter-indicator';
    let secondFighterBoard = new Fightner(secondFighter);
    secondFighterBoard.position = 'right-fighter-indicator';

    document.addEventListener('keydown', function (event) {
      switch (event.code) {
        case controls.PlayerOneAttack:
          putDamageHealth(getDamage(firstFighterBoard, secondFighterBoard), secondFighterBoard, firstFighterBoard);
          break;
        case controls.PlayerTwoAttack:
          putDamageHealth(getDamage(secondFighterBoard, firstFighterBoard), firstFighterBoard, secondFighterBoard);
          break;
        case controls.PlayerOneBlock:
          changeBlockPosition(firstFighterBoard, event.repeat);
          break;
        case controls.PlayerTwoBlock:
          changeBlockPosition(secondFighterBoard, event.repeat);
          break;
        case controls.PlayerOneCriticalHitCombination[0]:
          handlerCountExtraHit(firstFighterBoard, controls.PlayerOneCriticalHitCombination[0], true);
          putDamageHealth(
            getExtraDamage(firstFighterBoard, firstFighterBoard.helperCountExtraHit.length),
            secondFighterBoard,
            firstFighterBoard
          );
          break;
        case controls.PlayerOneCriticalHitCombination[1]:
          handlerCountExtraHit(firstFighterBoard, controls.PlayerOneCriticalHitCombination[1], true);
          putDamageHealth(
            getExtraDamage(firstFighterBoard, firstFighterBoard.helperCountExtraHit.length),
            secondFighterBoard,
            firstFighterBoard
          );
          break;
        case controls.PlayerOneCriticalHitCombination[2]:
          handlerCountExtraHit(firstFighterBoard, controls.PlayerOneCriticalHitCombination[2], true);
          putDamageHealth(
            getExtraDamage(firstFighterBoard, firstFighterBoard.helperCountExtraHit.length),
            secondFighterBoard,
            firstFighterBoard
          );
          break;
        case controls.PlayerTwoCriticalHitCombination[0]:
          handlerCountExtraHit(secondFighterBoard, controls.PlayerTwoCriticalHitCombination[0], true);
          putDamageHealth(
            getExtraDamage(secondFighterBoard, secondFighterBoard.helperCountExtraHit.length),
            firstFighterBoard,
            secondFighterBoard
          );
          break;
        case controls.PlayerTwoCriticalHitCombination[1]:
          handlerCountExtraHit(secondFighterBoard, controls.PlayerTwoCriticalHitCombination[1], true);
          putDamageHealth(
            getExtraDamage(secondFighterBoard, secondFighterBoard.helperCountExtraHit.length),
            firstFighterBoard,
            secondFighterBoard
          );
          break;
        case controls.PlayerTwoCriticalHitCombination[2]:
          handlerCountExtraHit(secondFighterBoard, controls.PlayerTwoCriticalHitCombination[2], true);
          putDamageHealth(
            getExtraDamage(secondFighterBoard, secondFighterBoard.helperCountExtraHit.length),
            firstFighterBoard,
            secondFighterBoard
          );
          break;
        default:
          'Unknown';
      }
      // resolve the promise with the winner when fight is over
      if (firstFighterBoard.health <= 0) {
        return resolve(secondFighter);
      } else if (secondFighterBoard.health <= 0) {
        return resolve(firstFighter);
      }
    });
    document.addEventListener('keyup', function (event) {
      switch (event.code) {
        case controls.PlayerOneBlock:
          changeBlockPosition(firstFighterBoard, event.repeat);
          break;
        case controls.PlayerTwoBlock:
          changeBlockPosition(secondFighterBoard, event.repeat);
          break;
        case controls.PlayerOneCriticalHitCombination[0]:
          handlerCountExtraHit(firstFighterBoard, controls.PlayerOneCriticalHitCombination[0], false);
          break;
        case controls.PlayerOneCriticalHitCombination[1]:
          handlerCountExtraHit(firstFighterBoard, controls.PlayerOneCriticalHitCombination[1], false);
          break;
        case controls.PlayerOneCriticalHitCombination[2]:
          handlerCountExtraHit(firstFighterBoard, controls.PlayerOneCriticalHitCombination[2], false);
          break;
        case controls.PlayerTwoCriticalHitCombination[0]:
          handlerCountExtraHit(secondFighterBoard, controls.PlayerTwoCriticalHitCombination[0], false);
          break;
        case controls.PlayerTwoCriticalHitCombination[1]:
          handlerCountExtraHit(secondFighterBoard, controls.PlayerTwoCriticalHitCombination[1], false);
          break;
        case controls.PlayerTwoCriticalHitCombination[2]:
          handlerCountExtraHit(secondFighterBoard, controls.PlayerTwoCriticalHitCombination[2], false);
          break;
        default:
          'Unknown';
      }
    });
  });
}

function getRandomArbitrary(min:number, max:number) {
  return Math.random() * (max - min) + min;
}

function changeBlockPosition(fighter:Fightner, repeat:boolean) {
  if (repeat) {
    return 0;
  }
  fighter.blockPosition = !fighter.blockPosition;
}

function changeHealthIndificator(levelHealth:number, defender:Fightner) {
  
  const indicatorHealsFighter = document.getElementById(`${defender.position}`);
  indicatorHealsFighter.style.width = `${levelHealth}%`;
}

function getCurrentLevelIndicatorHealth(currentHeals:number, defender:Fightner) {
  const levelHealth = (currentHeals * 100) / defender.primaryHealth;
  return levelHealth >= 0 ? levelHealth : 0;
}

function putDamageHealth(damage:number, defender:Fightner, attacker:Fightner) {
  if (attacker.blockPosition || (defender.blockPosition && attacker.helperCountExtraHit.length === 0)) {
    return 0;
  }
  defender.health = defender.health - damage;
  const healthLevel = getCurrentLevelIndicatorHealth(defender.health, defender);
  changeHealthIndificator(healthLevel, defender);
}

export function getDamage(attacker:Fightner, defender:Fightner) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage <= 0 ? 0 : damage;
}

export function getHitPower(fighter:Fightner) {
  const criticalHitChance = getRandomArbitrary(1, 2);
  const power = fighter.attack * criticalHitChance;
  return power;
}

function getExtraDamage(attacker:Fightner, value:number) {
  if (value === 3 && getExtraHitPower(attacker)) {
    const damage = 2 * attacker.attack;
    return damage;
  }
  return 0;
}

function getExtraHitPower(fighter:Fightner) {
  if (fighter.extraHitPower && !fighter.blockPosition) {
    fighter.extraHitPower = false;
    setTimeout(() => {
      fighter.extraHitPower = true;
    }, 10000);
    return true;
  }
  return false;
}

function handlerCountExtraHit(fighter:Fightner, key:string, bool:boolean) {
  const pos = fighter.helperCountExtraHit.indexOf(key as never);

  if (bool && pos < 0) {
    fighter.helperCountExtraHit.push(key as never);
    return fighter.helperCountExtraHit.length;
  } else if (!bool && pos >= 0) {
    fighter.helperCountExtraHit.splice(pos, 1);
    return fighter.helperCountExtraHit.length;
  }
}

export function getBlockPower(fighter:Fightner) {
  const dodgeChance = getRandomArbitrary(1, 2);
  const power = fighter.defense * dodgeChance;
  return power;
}
