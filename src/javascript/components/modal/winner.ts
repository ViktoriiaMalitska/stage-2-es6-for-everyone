import { showModal } from './modal';
import {createFighterImage} from '../fighterPreview';
import {SelectedFightersInterface} from "../arena";


export type WinnerType = {
  attack: number
  defense: number
  health: number
  name: string
  source: string
  _id: string
}

export function showWinnerModal(fighter:SelectedFightersInterface) {
  const bodyElement = createFighterImage(fighter);
  const modal = {
    title: fighter.name,
    bodyElement: bodyElement
  }
  showModal(modal)
}
