import { createElement } from '../helpers/domHelper';
import {SelectedFightersInterface} from './arena';

export function createFighterPreview(fighter:SelectedFightersInterface, position:string) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  let nameEl, attackEl, defenseEl, healthEl, imgEl;
  if (fighter) {
    nameEl = createElement({
      tagName: 'div',
    });
    nameEl.innerText = `Name: ${fighter.name}`;

    attackEl = createElement({
      tagName: 'div',
    });
    attackEl.innerText = `Attack: ${fighter.attack}`;

    defenseEl = createElement({
      tagName: 'div',
    });
    defenseEl.innerText = `defense ${fighter.defense}`;

    healthEl = createElement({
      tagName: 'div',
    });
    healthEl.innerText = `health ${fighter.health}`;

    const fighterElImg = createElement({ tagName: 'div', className: 'fighters___fighter' });
    const imageEl = createFighterImage(fighter);
    fighterElImg.append(imageEl);
    
    fighterElement.append(nameEl, attackEl, defenseEl, healthEl, fighterElImg);
  }
  // todo: show fighter info (image, name, health, etc.)
  return fighterElement;
}

export function createFighterImage(fighter:SelectedFightersInterface) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
